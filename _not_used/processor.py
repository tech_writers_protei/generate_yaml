from pathlib import Path

from loguru import logger

from child_dir import ChildDir
from child_dir_dict import ChildDirDict, child_dir_dict
from constants import DictPath, StrPath, _INDEX_FILE, base_path, stringify_dict_items
from full_storage import FullStorage, full_storage, StorageItem
from md_file import MdFile
from md_file_dict import MdFileDict, md_file_dict


class Processor:
    def __init__(self):
        self._md_file_dict: MdFileDict = md_file_dict
        self._child_dir_dict: ChildDirDict = child_dir_dict
        self._full_storage: FullStorage = full_storage
        self._dict_files: DictPath = dict()

    def __repr__(self):
        return f"{self.__class__.__name__}: {stringify_dict_items(self._dict_files)}"

    @property
    def base_path(self):
        return self._full_storage.base_path

    def set_full_storage(self):
        self._full_storage.set_dict_items()
        self._full_storage.removeindexes()
        self._full_storage.remove_repeats()

    def set_md_file_dict(self):
        storage_item: StorageItem
        for storage_item in self._full_storage.values():
            for path in storage_item.files:
                md_file: MdFile = MdFile(path)
                md_file.read()
                md_file.set_front_matter()
                self._md_file_dict + md_file
        return

    def set_child_dir_dict(self):
        dirpath: Path
        storage_item: StorageItem
        for dirpath, storage_item in iter(self._full_storage):
            weight: int = int(self.get_md_file(dirpath.joinpath(_INDEX_FILE)))
            child_dir: ChildDir = ChildDir(dirpath, weight, files=storage_item.files, subdirs=storage_item.subdirs)
            child_dir.remove_index()
            self._child_dir_dict + child_dir
        return

    def sort(self):
        child_dir: ChildDir
        for child_dir in iter(self._child_dir_dict):
            md_files: list[MdFile] = [self.get_md_file(item) for item in child_dir.full_files()]
            md_files.sort()
            self._dict_files[child_dir.dirpath] = [md_file.path for md_file in md_files]

    def to_dict(self, dirpath: Path = None, dictionary: DictPath = None) -> DictPath:
        if dirpath is None:
            dirpath: Path = self.base_path

        if dictionary is None:
            dictionary: DictPath = dict()
            dictionary[dirpath] = []

        for path in self._dict_files.get(dirpath):
            if path.is_file():
                dictionary[dirpath].append(path)
            elif path.is_dir():
                _dict = self.to_dict(path, dictionary)
                dictionary[dirpath].append(_dict)
            else:
                logger.info(f"Invalid path {path}")
                continue

        return dictionary

    def get_dict(self, dirpath: Path = None) -> dict[str, list[str | dict[str, list]]]:
        if dirpath is None:
            dirpath: Path = self.base_path
        dictionary: dict[str, list[str]] = dict()
        child_dir: ChildDir = self.get_child_dir(dirpath)
        dictionary[child_dir.yaml_path] = []

        for path in self._dict_files.get(dirpath):
            md_file: MdFile = self.get_md_file(path)
            dictionary[child_dir.yaml_path].append(md_file.yaml_path)

        index: int
        for index, item in enumerate(dictionary[child_dir.yaml_path]):
            if isinstance(item, str) and item.endswith(_INDEX_FILE):
                dictionary[child_dir.yaml_path][0] = ""
                dictionary[child_dir.yaml_path][index] = dictionary.get(item)

        return dictionary

    def get_md_file(self, path: StrPath):
        return self._md_file_dict.get(path)

    def get_child_dir(self, path: StrPath):
        return self._child_dir_dict.get(path)

    def all_paths(self) -> set[Path]:
        return set(path for v in self._full_storage.values() for path in v.paths)

    def __contains__(self, item):
        if isinstance(item, StrPath):
            return Path(item) in self.all_paths()
        elif isinstance(item, MdFile):
            return item.path in self._md_file_dict
        elif isinstance(item, ChildDir):
            return item.dirpath in self._child_dir_dict
        else:
            return False

    def stringify_to_dict(self):
        return stringify_dict_items(self.to_dict())


processor: Processor = Processor()

if __name__ == '__main__':
    full_storage.base_path = base_path
    processor.set_full_storage()
    processor.set_md_file_dict()
    processor.set_child_dir_dict()

    processor.sort()
    logger.info(f"\n{processor.stringify_to_dict()}")
