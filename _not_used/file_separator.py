from pathlib import Path
from typing import Any, Iterator, NamedTuple

from loguru import logger

from child_dir import ChildDir
from constants import DirYAML, TitleYAML, _INDEX_FILE, StrPath
from errors import BaseYAMLError


class ChildDirElement(NamedTuple):
    child_dir: ChildDir
    files: list[Path] = []
    index: int = 0

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.child_dir.path_dir}"

    def path(self):
        if self.index > 0:
            return f"{self.child_dir.path_dir.relative_to(ChildDir._base_path)}{self.index}"
        else:
            return f"{self.child_dir.path_dir.relative_to(ChildDir._base_path)}"

    def title_yaml(self) -> TitleYAML:
        if self.child_dir.level > 2:
            level: int | None = self.child_dir.level
        else:
            level: int | None = None

        return TitleYAML(None, level)

    def dir_yaml(self) -> DirYAML:
        # logger.info(f"child_dir_element = {self.child_dir}")
        # logger.info(f"child_dir_element = {type(self.child_dir)}")
        files: list[str] | None = [
            _md_file.yaml_path for _md_file in self.child_dir.iter_md_files()
        ] if self.child_dir.files else None

        dir_yaml: DirYAML = DirYAML(self.title_yaml(), None, files)

        try:
            dir_yaml.validate()
        except BaseYAMLError:
            logger.error(f"Для директории {self.child_dir.path_dir} не удалось сгенерировать раздел YAML-файла")
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}, {e.strerror}")
            logger.error(f"Для директории {self.child_dir.path_dir} не удалось сгенерировать раздел YAML-файла")
        else:
            return dir_yaml

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.child_dir, self.index == other.child_dir, other.index
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.child_dir, self.index != other.child_dir, other.index
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self.child_dir == other.child_dir:
            return self.index < other.index
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self.child_dir == other.child_dir:
            return self.index > other.index
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__) and self.child_dir == other.child_dir:
            return self.index <= other.index
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__) and self.child_dir == other.child_dir:
            return self.index >= other.index
        else:
            return NotImplemented


class FileSeparator:
    _index: int = 0

    def __init__(self, base_path: StrPath):
        self._base_path: Path = Path(base_path).resolve()
        self._child_dir: ChildDir | None = None
        self._index_file_nums: list[int] = []
        self._child_dir_elements: list[ChildDirElement] = []

    def __bool__(self):
        return self._child_dir is not None

    def __len__(self):
        return len(self._index_file_nums)

    def __iter__(self) -> Iterator[ChildDirElement]:
        return iter(self._child_dir_elements)

    def nullify(self):
        self._child_dir = None
        self._index_file_nums = []
        self._child_dir_elements: list[ChildDirElement] = []
        self.__class__._index = 0

    def set_indexes(self):
        for idx, md_file in enumerate(self._child_dir.iter_md_files()):
            # logger.info(f"md file = {str(md_file)}")
            if md_file.path.name == _INDEX_FILE:
                self._index_file_nums.append(idx)

    def split_file_dir(self):
        if len(self) == 0:
            return

        if len(self._child_dir) - 1 not in self._index_file_nums:
            self._index_file_nums.append(len(self._child_dir) - 1)
        logger.info(f"index_file_nums = {self._index_file_nums}")

        for index, num in enumerate(self._index_file_nums[:-1]):
            # logger.info(f"index = {index}")
            start: int | None = num if len(self) > 0 else None
            logger.info(f"start = {start}")
            end: int | None = self._index_file_nums[index + 1] if index < len(self) else None
            logger.info(f"end = {end}")
            md_files: list[Path] = [md_file for md_file in self._child_dir.get_files(start=start, end=end)]
            child_dir_element: ChildDirElement = ChildDirElement(self._child_dir, md_files, index)
            self._child_dir_elements.append(child_dir_element)
        logger.info(f"len = {len(self)}")

    def remove_md_files(self):
        for index_file_num in reversed(self._index_file_nums):
            _paths: str = ", ".join(str(_) for _ in self._child_dir.files)
            # logger.info(f"child = {_paths}")
            # logger.info(f"index_file_num = {index_file_num}")
            self._child_dir.files.pop(index_file_num)
        for child_dir_element in iter(self):
            for file in child_dir_element.files:
                self._child_dir - file

    def sort_child_dir_elements(self):
        logger.info(f"_child_dir_elements = {', '.join(str(_) for _ in self._child_dir_elements)}")
        self._child_dir_elements.sort()
        logger.info(f"_child_dir_elements = {', '.join(str(_) for _ in self._child_dir_elements)}")

    def yamlify(self):
        yaml: dict[str, Any] = dict()
        # core_path: Path = ChildDir.core_child_dir().path_dir
        _pre_yaml_child_dir: dict[str, Any] = self._child_dir.dir_yaml.yamlify()
        _yaml_child_dir: dict[str, dict] = {
            str(self._child_dir.path_dir.relative_to(self._base_path)): _pre_yaml_child_dir}

        # logger.error(f"_yaml = {_yaml_child_dir}")

        yaml.update(_yaml_child_dir)

        # logger.info(f"_yaml_child_dir = {_yaml_child_dir}")
        # logger.info(f"len = {len(self)}")
        # logger.info(f"child_dir_elements = {self._child_dir_elements}")

        for child_dir_element in iter(self):
            _pre_yaml_child_element: dict[str, Any] = child_dir_element.dir_yaml().yamlify()
            _yaml_child_element: dict[str, dict] = {
                f"{child_dir_element.path()}": _pre_yaml_child_element
            }
            yaml.update(_yaml_child_element)
            logger.info(f"_yaml_child_element = {_yaml_child_element}")
        return yaml

    def prepare(self):
        # logger.info(f"_child_dir = {str(self._child_dir)}")
        if self._child_dir.path_dir == self._base_path:
            return dict()
        else:
            self.set_indexes()
            self.split_file_dir()
            self.remove_md_files()
            return self.yamlify()

    @property
    def child_dir(self):
        return self._child_dir

# file_separator: FileSeparator = FileSeparator(ChildDir._base_path)
