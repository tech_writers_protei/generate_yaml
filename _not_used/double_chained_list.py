from __future__ import annotations

from enum import Enum
from typing import Any, Iterable

from loguru import logger


class ItemError(Exception):
    """Base error class to inherit."""


class ItemIndexError(ItemError):
    """Invalid item index."""


class ItemBorderValueError(ItemError):
    """Value cannot be reassigned."""


class TooManyItemsBorderIndexError(ItemError):
    """Too many items with the index for the border one."""


class MissingItemBorderIndexError(ItemError):
    """No item with the index for the border one."""


class InvalidHeadTailNumberError(ItemError):
    """Head and Tail must be the only items."""


class InvalidAddElementError(ItemError):
    """Adding element must be of the Item type."""


class ItemBorder(Enum):
    TAIL = -2
    HEAD = -1
    NONE = 0

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    __repr__ = __str__


class ItemAttribute(Enum):
    BEFORE = "before"
    AFTER = "after"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    __repr__ = __str__


class Item:
    _index: int = 0
    items: dict[int, 'Item'] = {}

    def __init__(
            self,
            obj: Any, *,
            before: int = None,
            after: int = None,
            border: ItemBorder = ItemBorder.NONE):
        self._obj: Any = obj

        if border == ItemBorder.NONE:
            if before is None:
                before: int = -1
            if after is None:
                after: int = -2

            self._identifier: int = self.__class__._index
            self.__class__._index += 1

        else:
            self._identifier = border.value

        self._before: int | None = before
        self._after: int | None = after
        self.__class__.items[self._identifier] = self

    def __str__(self):
        return f"{self._before} <- {str(self._obj)}, {self._identifier} -> {self._after}"

    def __repr__(self):
        return (
            f"{repr(self._obj)}, identifier = {self._identifier}\n"
            f"before = {self._before}, after = {self._after}")

    def __bool__(self):
        return True

    def __getattr__(self, item):
        return getattr(self._obj, item)

    def get_before(self):
        return self.__class__.items.get(self._before, None)

    def get_after(self):
        return self.__class__.items.get(self._after, None)

    @property
    def is_first(self) -> bool:
        return self._before == -1

    @property
    def is_last(self) -> bool:
        return self._after == -2

    @property
    def after(self) -> int:
        return self._after

    @after.setter
    def after(self, value):
        if isinstance(value, int):
            self._after = value
        else:
            logger.error(
                f"Индекс {value} последующего файла должен быть int, но получен {type(value)}")
            raise ItemIndexError

    @property
    def before(self) -> int:
        return self._before

    @before.setter
    def before(self, value):
        if isinstance(value, int):
            self._before = value
        else:
            logger.error(
                f"Индекс {value} предыдущего файла должен быть int, но получен {type(value)}")
            raise ItemIndexError

    @property
    def identifier(self):
        return self._identifier

    @classmethod
    def check_identifier(cls, identifier: int) -> bool:
        return identifier in cls.items


class Head(Item):
    def __init__(self):
        obj: Any = None
        before = None
        after: int = -2
        border: ItemBorder = ItemBorder.HEAD
        super().__init__(
            obj,
            before=before,
            after=after,
            border=border)

    def __str__(self):
        return f"Head -> {self._after}"

    def __repr__(self):
        return f"after = {self._after}"

    def __bool__(self):
        return False

    @property
    def before(self):
        return self._before

    @before.setter
    def before(self, value):
        logger.error("Элементу Head нельзя переназначить значение before")
        raise ItemBorderValueError


head: Head = Head()


class Tail(Item):
    def __init__(self):
        before: int = -1
        after = None
        obj: Any = None
        border: ItemBorder = ItemBorder.TAIL
        super().__init__(
            obj,
            before=before,
            after=after,
            border=border)

    def __str__(self):
        return f"{self._before} -> Tail"

    def __repr__(self):
        return f"before = {self._before}"

    def __bool__(self):
        return False

    @property
    def after(self):
        return self._after

    @after.setter
    def after(self, value):
        logger.error("Элементу Tail нельзя переназначить значение after")
        raise ItemBorderValueError


tail: Tail = Tail()


class DoubleChainedList:
    def __init__(self):
        self._values: list[Item] = []
        self._values.extend((head, tail))

    def __len__(self):
        return len(self.items)

    def __iter__(self):
        return iter(_ for _ in self._values if not bool(_))

    def __getitem__(self, item):
        if isinstance(item, int):
            return self.items[item]
        else:
            raise KeyError

    def head(self) -> Head:
        return self[-1]

    def tail(self) -> Tail:
        return self[-2]

    @property
    def items(self) -> tuple[Item, ...]:
        return tuple(iter(self))

    def _find_item(self, index: int, attr: ItemAttribute):
        _items: list[Item] = list(
            filter(lambda x: getattr(x, attr.value) == index, self.items))

        if not _items:
            logger.error(f"Индекс {index} для атрибута {attr.value} не найден")
            return None
        elif len(_items) > 1:
            logger.error(
                f"Найдено несколько файлов с индексом {index} для атрибута {attr.value}")
            raise TooManyItemsBorderIndexError
        else:
            return _items[0]

    def _set_head(self):
        if len(self) == 0:
            return
        _first_item: Item | None = self._find_item(-1, ItemAttribute.BEFORE)
        if _first_item is None:
            raise MissingItemBorderIndexError
        self.head().after = _first_item.identifier
        return _first_item

    def _set_tail(self):
        if len(self) == 0:
            return
        _last_item: Item | None = self._find_item(-2, ItemAttribute.AFTER)
        if _last_item is None:
            raise MissingItemBorderIndexError
        self.tail().before = _last_item.identifier

    def __add__(self, other):
        if not isinstance(other, (Iterable, Item)):
            logger.error(f"Тип {other} должен быть Item или Iterabnle, но получен {type(other)}")
            raise InvalidAddElementError
        elif isinstance(other, (Head, Tail)):
            logger.error("Нельзя добавить элементы Head и Tail")
            raise InvalidHeadTailNumberError

        if not Item.check_identifier(other.identifier):
            logger.error(f"Идентификатор {other} не найден")
            raise ItemIndexError

        other.get_before().before = other.identifier
        other.get_after().after = other.identifier

        self._values.append(other)

    __radd__ = __add__
    __iadd__ = __add__
