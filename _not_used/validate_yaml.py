from typing import Any, Mapping, Sequence

import yaml
from loguru import logger
from yaml.error import YAMLError

from constants import StrPath

_TITLE_LOGO: str = "images/logo.svg[top=4%,aligh=right,pdfwidth=3cm]"


def any_bools(bools: Sequence[bool]):
    for index, _ in enumerate(bools):
        if _:
            logger.error(f"The requirement failed:\n{bools[index].__name__}")
            raise ValueError


def validate_yaml(yaml_file: StrPath):
    try:
        with open(yaml_file, "r") as f:
            content: dict[str, Any] = yaml.safe_load(f)
    except YAMLError:
        logger.error(f"Invalid YAML file")
        raise
    else:
        return content


def validate_keys(content: Mapping[str, Any]):
    _settings: bool = "settings" not in content
    _rights: bool = all(_ not in content for _ in ("Rights", "rights"))
    _title_logo_image: bool = "title-logo-image" not in content["settings"]
    _title_page: bool = "title-page" not in content["settings"]
    _title_page_value: bool = content["settings"]["title-page"] == "{{ProjectName}}"
    _version: bool = "version" not in content["settings"]
    _version_value: bool = content["settings"]["version"] == "{{Version}}"
    _title_logo_image_value: bool = content["settings"]["title-logo-image"] != _TITLE_LOGO
