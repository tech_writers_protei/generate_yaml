from pathlib import Path
from typing import Iterable

from loguru import logger

from child_dir import ChildDir
from constants import ChildDirItem, _INDEX_FILE


def _stringify_path(paths: Iterable[Path]) -> list[str]:
    return [str(path) for path in paths]


class Separator:
    _base_path: Path | None = None

    def __init__(self):
        self._child_dir: ChildDir | None = None
        self._index: int = 0

    def __repr__(self):
        return f"{self.__class__.__name__}: {repr(self._child_dir)}"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._child_dir.dirpath}"

    def __bool__(self):
        return self._child_dir is not None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def nullify(self):
        self._child_dir: ChildDir | None = None
        self._index: int = 0

    def generate_item(self, split_files: Iterable[Path] = None):
        if split_files is None:
            return

        for split_file in split_files:
            self._child_dir - split_file

        child_dir_item: ChildDirItem = ChildDirItem(
            f"{self.__class__._base_path}",
            f"{self._child_dir.dirpath}",
            self._index - 1,
            _stringify_path(split_files),
            self._child_dir.level)
        self._child_dir.child_dir_items.append(child_dir_item)
        logger.warning(f"child_dir_item base_path = {child_dir_item.base_path}")
        logger.warning(f"child_dir_item path = {child_dir_item.name}")
        return

    # FIXME
    def split(self):
        if len(self._child_dir) == 0:
            return

        _indexes: list[int] = [
            len(self._child_dir) - index for index, file in enumerate(iter(self._child_dir))
            if file.name == _INDEX_FILE]
        _indexes.sort(reverse=True)
        _len_indexes: int = len(_indexes)
        self._index = _len_indexes

        for _index in _indexes:
            split_files: list[Path] = self._child_dir[_index:]
            self.generate_item(split_files)
            self._child_dir - split_files
            self._index -= 1

        return


separator: Separator = Separator()
