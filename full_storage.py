from os import walk
from pathlib import Path
from typing import Iterable, Iterator, Mapping, Sequence

from loguru import logger

from child_dir import ChildDir
from child_dir_dict import ChildDirDict, child_dir_dict
from constants import DictStr, INDEX_FILE, StrPath, _EXTENSION, _INDEX_FILE, base_path, is_bool
from errors import InvalidBaseDirPathError, InvalidDirPathError, InvalidMdFileTypeError, MissingBaseDirPathError
from md_file import MdFile
from md_file_dict import MdFileDict, md_file_dict


def validate_path_dir(path: StrPath) -> Path:
    if isinstance(path, str):
        path: Path = Path(path)
    if not path.exists():
        logger.error(f"Path {path} is not found")
        raise MissingBaseDirPathError
    elif not path.is_dir():
        logger.error(f"Path {path} is invalid")
        raise InvalidBaseDirPathError
    else:
        return path.resolve()


def get_dir_tree_str(path: Path):
    _dict: DictStr = dict()

    for dirname, subdirs, files in walk(path, followlinks=True):
        _dirname: str = str(Path(dirname).resolve())
        _dict[_dirname] = []

        for file in files:
            _file: Path = path.joinpath(file)
            if path.joinpath(file).suffix != _EXTENSION:
                continue
            elif path.joinpath(file).name == _INDEX_FILE:
                continue
            else:
                _dict[_dirname].append(str(_file))

        for subdir in subdirs:
            _subdir: Path = path.joinpath(subdir).resolve()
            if not any(_path.suffix == _EXTENSION for _path in _subdir.iterdir()):
                continue
            elif _subdir.joinpath(INDEX_FILE).exists():
                _dict[_dirname].append(str(_subdir.joinpath(INDEX_FILE)))
            else:
                _dict[_dirname].append(get_dir_tree_str(_subdir))

        return _dict


class FullStorage:
    def __init__(
            self, *,
            ignored_dirs: Iterable[str] = None,
            ignored_files: Iterable[str] = None):
        if ignored_dirs is None:
            ignored_dirs: list[str] = []
        if ignored_files is None:
            ignored_files: list[str] = []

        self._base_path: Path | None = None
        self._items: DictStr = dict()
        self._md_file_dict: MdFileDict = md_file_dict
        self._child_dir_dict: ChildDirDict = child_dir_dict
        self._ignored_dirs: set[str] = {*ignored_dirs}
        self._ignored_files: set[str] = {*ignored_files}

    def __bool__(self):
        return self._base_path is not None

    def __str__(self):
        _str: list[str] = [f"  {k}:\n{v}" for k, v in self._items.items()]
        return "\n".join(_str)

    __repr__ = __str__

    def get_item(
            self,
            keys: Sequence,
            dictionary: DictStr = None):
        if dictionary is None:
            dictionary: DictStr = self._items

        if not keys or dictionary is None:
            return dictionary
        else:
            return self.get_item(keys[1:], dictionary.get(keys[0]))

    get = get_item

    def __delitem__(self, key):
        del self._items[key]

    def __iter__(self) -> Iterator[tuple[str, list[str | dict]]]:
        return iter((k, v) for k, v in self._items.items())

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def add_ignored_file(self, file: str):
        self._ignored_files.add(file)

    def add_ignored_dir(self, directory: str):
        self._ignored_dirs.add(directory)

    # def move_element(self, key: str, from_index: int, to_index: int):
    #     self.get(key).insert(to_index, self.get(key).pop(from_index))

    # def replace_element(self, path: Path):
    #     md_file: MdFile = self._md_file_dict.get(path)
    #     child_dir: ChildDir = self._child_dir_dict.get(path.parent)

    @property
    def base_path(self):
        return self._base_path

    @base_path.setter
    def base_path(self, value):
        if isinstance(value, StrPath):
            value: Path = validate_path_dir(value)
            self._base_path = value
        else:
            logger.error(f"Base path {value} must be str or Path but {type(value)} received")
            raise InvalidDirPathError

    def values(self) -> tuple[Path | dict, ...]:
        return tuple(*self._items.values())

    def items(self):
        return self._items.items()

    @is_bool
    def get_dir_tree(self, path: Path = None):
        if path is None:
            path: Path = self._base_path

        _dict: DictStr = dict()

        for dirname, subdirs, files in walk(path, followlinks=True):
            _dirname: str = str(Path(dirname).resolve())
            _dict[_dirname] = []

            for file in files:
                _file: Path = path.joinpath(file)
                if path.joinpath(file).suffix != _EXTENSION:
                    continue
                elif path.joinpath(file).name == _INDEX_FILE:
                    continue
                else:
                    _dict[_dirname].append(str(_file))

            for subdir in subdirs:
                _subdir: Path = path.joinpath(subdir).resolve()
                if not any(_path.suffix == _EXTENSION for _path in _subdir.iterdir()):
                    continue
                elif _subdir.joinpath(INDEX_FILE).exists():
                    _dict[_dirname].append(str(_subdir.joinpath(INDEX_FILE)))
                else:
                    _dict[_dirname].append(get_dir_tree_str(_subdir))

            return _dict

    def set_dir_tree(self):
        self._items.update(self.get_dir_tree())

    def set_dicts(self, dictionary: Mapping[str, list[str | dict]] = None):
        if dictionary is None:
            dictionary = self._items

        for k, v in dictionary.items():
            _file_path: Path = Path(k)
            _md_file: MdFile = MdFile(_file_path.joinpath(_INDEX_FILE))
            _md_file.read()
            _md_file.set_front_matter()
            self._md_file_dict + _md_file

            child_dir: ChildDir = ChildDir(_file_path, int(_md_file))
            self._child_dir_dict + child_dir

            if _file_path.parent in self._child_dir_dict:
                self._child_dir_dict.get(_file_path.parent) + child_dir

            for value in v:
                if isinstance(value, str):
                    md_file: MdFile = MdFile(value)
                    md_file.read()

                    if not md_file.is_empty:
                        md_file.set_front_matter()
                        self._md_file_dict + md_file
                        self._child_dir_dict.get(_file_path) + md_file

                elif isinstance(value, dict):
                    self.set_dicts(value)

                else:
                    logger.error(f"Invalid type {value}")
                    raise InvalidMdFileTypeError

        return

    def get_md_files(
            self,
            dictionary: Mapping[str, list[str, dict[str, list]]] = None,
            md_files: Iterable[MdFile] = None):
        if dictionary is None:
            dictionary: dict[str, list[str, dict[str, list]]] = self._items

        if md_files is None:
            md_files: dict[MdFile, list[MdFile]] = dict()

        k: str
        v: list[str | dict[str, list]]
        for k, v in dictionary.items():
            _key: MdFile = self._md_file_dict.get(Path(k).joinpath(_INDEX_FILE))
            md_files[_key] = []

            for value in v:
                if isinstance(value, MdFile):
                    _md_file: MdFile = self._md_file_dict.get(value)
                    md_files[_key].append(_md_file)

                elif isinstance(value, dict):
                    logger.info(f"value = {value}")
                    key: str = tuple(value.keys())[0]
                    _md_file: MdFile = self._md_file_dict.get(Path(key).joinpath(_INDEX_FILE))
                    md_files[_key].append(_md_file)

                else:
                    logger.error(f"Invalid type {type(value)} of the value {value}")
                    continue

        return md_files

    def sort_md_files(self, md_file: StrPath | MdFile = None):
        if md_file is None:
            md_file: MdFile = self._md_file_dict.get(self._base_path.joinpath(_INDEX_FILE))

        if isinstance(md_file, StrPath):
            md_file: MdFile = self._md_file_dict.get(md_file)

        md_files = self.get_md_files().get(md_file)
        md_files.sort(key=lambda x: int(x))
        return md_files

    # FIXME
    def sort_recursively(self, dictionary: Mapping[MdFile, list[MdFile]] = None):
        if dictionary is None:
            _md_file: MdFile = self._md_file_dict.get(self._base_path.joinpath(_INDEX_FILE))
            dictionary: dict[MdFile, list[MdFile]] = {_md_file: self.sort_md_files(_md_file)}

        _sorted: dict[MdFile, list[MdFile | dict]] = dict()

        for key, md_files in dictionary.items():
            _sorted[key] = []
            _replace: dict[MdFile, int] = dict()

            for index, md_file in enumerate(md_files):
                if md_file.path.name == _INDEX_FILE:
                    _replace[md_file] = index
                _sorted[key].append(md_file)

            for k, v in _replace.items():
                _sorted[key][v] = self.s






    def sort(self):
        _sorted: dict[MdFile, list[MdFile | dict]] = dict()
        _key: MdFile = self._md_file_dict.get(self._base_path)
        _md_files: list[MdFile] = self.sort_md_files()
        _sorted[_key] = _md_files

        for index, md_file in enumerate(_sorted[_key]):
            if md_file.path.name == _INDEX_FILE:
                _sorted[_key].pop(index)
                _sorted[_key].insert(index, self.sort_md_files(md_file))




        # for k, v in iter(self):
            # _k: MdFile = self._md_file_dict.get(k)
            # _sorted[_k] = self.get_md_files()
            # for value in v:
            #     if isinstance(value, Mapping):
            #         pass















full_storage: FullStorage = FullStorage()



if __name__ == '__main__':
    full_storage: FullStorage = FullStorage()
    full_storage.base_path = base_path
    full_storage.set_dir_tree()
    full_storage.set_dicts()
    # print(full_storage._items)
    # print(full_storage._md_file_dict)
    # print(list(iter(full_storage._child_dir_dict)))
    logger.info(full_storage.get_md_files())
    logger.info(full_storage.sort_md_files())
    # print(full_storage.get_items())
    # print(get_dir_tree(base_path))

    # print(json.dumps(get_dir_tree_str(base_path), indent=2, sort_keys=False))
    # print(json.dumps(get_dir_tree(base_path), indent=2, sort_keys=False))
