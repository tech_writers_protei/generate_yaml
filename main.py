from pathlib import Path

from loguru import logger

from child_dir import ChildDir
from custom_logger import configure_custom_logging
from full_storage import full_storage


# @configure_custom_logging(Path(__file__).name)
# def generate(project_name: str, version: str):
#     path: Path = Path("/Users/user/PycharmProjects/SGW/content/common")
#     # path: Path = Path(r"C:\Users\tarasov-a\PycharmProjects\SGW\content\common")
#     full_storage.base_path = path
#     ChildDir._base_path = path
#     Separator._base_path = path
#     full_storage.prepare()
#
#     yaml_file: FileYAML = FileYAML(Path(__file__).with_name("test_yaml.yaml"))
#     yaml_file.prepare()
#     yaml_file.replace("ProjectName", project_name)
#     yaml_file.replace("Version", version)
#
#     for dirpath, files in iter(full_storage):
#         for file_path in files:
#             md_file: MdFile = MdFile(file_path)
#             md_file.prepare()
#         child_dir: ChildDir = ChildDir(dirpath, files)
#         child_dir.sort()
#
#     for path, _ in iter(full_storage):
#         logger.info(f"main path = {path}")
#         child_dir: ChildDir = ChildDir.get_item(path)
#         separator._child_dir = child_dir
#         logger.info(f"child_dir = {repr(child_dir)}")
#         logger.info(f"{ChildDir._instances}")
#         separator.split()
#         yaml_file + child_dir.prepare()
#         separator.nullify()
#
#     yaml_file.write()
from md_file import MdFile


@configure_custom_logging(Path(__file__).parent.name)
def attempt():
    # path: Path = Path("/Users/user/PycharmProjects/SGW/content/common")
    path: Path = Path(r"C:\Users\tarasov-a\PycharmProjects\SGW\content\common")
    full_storage.base_path = path
    ChildDir._base_path = path

    full_storage.prepare()
    logger.info(list(iter(full_storage)))

    _md_files: list[Path] = [_v for _, v in iter(full_storage) for _v in v if _v.is_file()]
    for item in _md_files:
        md_file: MdFile = MdFile(item)
        md_file.prepare()

    for dirpath, items in iter(full_storage):
        files = []
        subdirs = []
        for item in items:
            if item.is_dir():
                subdirs.append(item)
                # _child_dir: ChildDir = ChildDir(dirpath, full_storage.get_files(item), full_storage.get_subdirs(item))
                # _child_dir.prepare()
            elif item.is_file():
                files.append(item)
                # md_file: MdFile = MdFile(item)
                # md_file.prepare()
        logger.info(f"files = {files}")
        logger.info(f"subdirs = {subdirs}")
        _: ChildDir = ChildDir(dirpath, files, subdirs)

    for child_dir_path in full_storage._items:
        child_dir: ChildDir = ChildDir.get_item(child_dir_path)
        child_dir.remove_index()
        child_dir.replaceindexes()
        # child_dir.prepare()
        # logger.info(f"to_dict = {child_dir.to_dict()}")
    core = ChildDir.get_item(path)
    logger.info(f"to_dict = {core.to_dict()}")


if __name__ == '__main__':
    # generate("SGW", "1.0.0")
    attempt()
