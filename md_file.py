from pathlib import Path
from typing import Any
from os.path import relpath
from loguru import logger

from constants import StrPath
from errors import InvalidFileStructureError, InvalidFrontMatterEntityError, InvalidFrontMatterError, \
    InvalidFrontMatterLineError, MissingFrontMatterError, MissingRequiredComponentsError
from front_matter import FrontMatter

_REQUIRED_PARTS: tuple[str, ...] = ("content", "common")
_BOOL: tuple[str | None, ...] = ("true", "false", None)


def process_line(line: str) -> str:
    return line.removesuffix("\n").strip()


def parse_front_matter(line: str) -> tuple[str, str]:
    if ":" not in line:
        logger.error(f"Line {line} does not have a colon")
        raise InvalidFrontMatterLineError
    _: list[str] = line.split(":")
    key: str = _[0].strip()
    value: str = _[1].strip()
    return key, value


def intify(value: str | None) -> int | None:
    if value is None:
        return value
    elif not value.isnumeric():
        logger.error(f"Value {value} is not numeric")
        return None
    else:
        return int(value)


def boolify(value: str | None) -> bool:
    if value == "true":
        return True
    elif value not in _BOOL:
        logger.error(f"Value {value} is not boolean")
        return False
    else:
        return False


class MdFile:
    def __init__(self, path: StrPath):
        self._path: Path = Path(path).resolve()
        self._front_matter: FrontMatter | None = None
        self._is_empty: bool | None = None

    def read(self):
        with open(self._path, "r", encoding="utf-8") as f:
            content: list[str] = f.readlines()

        indexes: list[int] = [
            index for index, line in enumerate(iter(content))
            if process_line(line) == "---"]

        if not indexes:
            logger.error(f"Файл {self._path} пуст")
            raise MissingFrontMatterError

        if len(indexes) < 2:
            logger.error(f"Количество строк '---' должно быть 2, но найдено {len(indexes)}")
            logger.error(f"Файл {self._path}")
            raise InvalidFileStructureError

        if indexes[0] != 0:
            logger.error(f"Самая первая строка должна быть '---', но найдено {content[0]}")
            raise InvalidFileStructureError

        max_non_empty_line_index = max(
            index for index, line in enumerate(iter(content)) if process_line(line))
        self._is_empty = not max_non_empty_line_index > indexes[1]

        return tuple(content[idx] for idx in range(1, indexes[1] - 1))

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __bool__(self):
        return self._front_matter is not None

    def __int__(self):
        if not bool(self):
            logger.error(f"Front matter не задано")
            logger.error(f"file = {self._path}")
            raise InvalidFrontMatterError
        else:
            return int(self._front_matter)

    def __hash__(self):
        return hash(self._path)

    def __key(self):
        return self.parent, int(self), self.name

    @property
    def parent(self):
        return self._path.parent

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name <= other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name >= other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name < other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name > other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    @property
    def name(self) -> str:
        return self._path.stem

    def set_front_matter(self):
        front_matter_dict: dict[str, Any] = dict()

        for line in self.read():
            if not line.removesuffix("\n"):
                continue
            else:
                key, value = parse_front_matter(line)
                if key in FrontMatter._fields:
                    front_matter_dict[key] = value

        if "title" not in front_matter_dict:
            logger.error(f"Ключ 'title' должен быть в front matter")
            raise InvalidFrontMatterEntityError

        _title: str = front_matter_dict.get("title")
        _description: str | None = front_matter_dict.get("description", None)
        _weight: int | None = front_matter_dict.get("weight", None)
        _type: str | None = front_matter_dict.get("type", None)
        _draft: bool | None = front_matter_dict.get("draft", None)

        self._front_matter = FrontMatter(
            _title.strip("\""),
            _description.strip("\""),
            intify(_weight),
            _type,
            boolify(_draft))

    @property
    def front_matter(self):
        return self._front_matter

    @property
    def yaml_path(self) -> str:
        if not all(_required in self._path.parts for _required in _REQUIRED_PARTS):
            logger.error(f"Отсутствуют обязательные компоненты: {_REQUIRED_PARTS}")
            raise MissingRequiredComponentsError

        for _parent in self._path.parents:
            if _parent.name == "content":
                logger.error(f"_parent = {_parent}")
                logger.error(f"yaml path = {relpath(self._path, _parent.parent)}")
                return relpath(self._path, _parent.parent)

    @property
    def path(self):
        return self._path

    @property
    def is_empty(self):
        return self._is_empty
