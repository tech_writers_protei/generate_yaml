class BaseError(Exception):
    """The base error class to inherit."""


class InvalidAttributeError(BaseError):
    """Attribute is not proper."""


class MissingFrontMatterError(BaseError):
    """File has no front matter."""


class InvalidFileStructureError(BaseError):
    """YAML borders are placed in wrong lines."""


class InvalidFrontMatterLineError(BaseError):
    """Line of the front matter does not have a colon."""


class InvalidFrontMatterEntityError(BaseError):
    """Front matter does not have a title key."""


class InvalidFrontMatterError(BaseError):
    """Front matter cannot be None."""


class InvalidDirPathError(BaseError):
    """Directory path is invalid."""


class BaseYAMLError(BaseError):
    """Base YAML error class to inherit."""


class MissingRequiredComponentsError(BaseError):
    """Path does not have required components."""


class MissingRequiredYAMLKeyError(BaseYAMLError):
    """YAML file does not have required key."""


class MissingBaseDirPathError(BaseError):
    """Base directory path does not exist."""


class InvalidBaseDirPathError(BaseError):
    """Base directory path does not lead to the folder."""


class MissingBoolMethodError(BaseError):
    """Object is not specified or does not have a __bool__ method."""


class InvalidMdFileTypeError(BaseError):
    """Dictionary value must be str or dict."""
