from pathlib import Path
from typing import Iterator

from loguru import logger

from constants import StrPath
from md_file import MdFile


class MdFileDict:
    base_path: Path | None = None

    def __init__(self):
        self._md_files: dict[Path, MdFile] = dict()

    def __repr__(self):
        _items: str = "\n".join([f"{k}: {v}" for k, v in self._md_files.items()])
        return f"{self.__class__.__name__}:\n{_items}"

    __str__ = __repr__

    def __len__(self):
        return len(self._md_files)

    def __contains__(self, item):
        if isinstance(item, StrPath):
            return Path(item) in self._md_files.keys()
        elif isinstance(item, MdFile):
            return item in self._md_files.values()
        else:
            return False

    def __iter__(self) -> Iterator[MdFile]:
        return iter(self._md_files.values())

    def __add__(self, other):
        if isinstance(other, MdFile):
            self._md_files[other.path] = other
        elif isinstance(other, StrPath):
            other: Path = Path(other)
            if not other.is_file():
                logger.error(f"Path {other} does not lead to the file")
            else:
                _md_file: MdFile = MdFile(other)
                self._md_files[other] = _md_file
        else:
            logger.error(f"Added element {other} must be MdFile or StrPath but {type(other)} received")

    def __sub__(self, other):
        def _sub(path: Path):
            if path not in self._md_files:
                logger.error(f"File {path} is not found")
            else:
                self._md_files.pop(path)

        if isinstance(other, MdFile):
            other: Path = other.path
        elif isinstance(other, StrPath):
            other: Path = Path(other)
        else:
            logger.error(f"Subtracted element {other} must be MdFile or StrPath but {type(other)} received")
            return

        _sub(other)

    def __getitem__(self, item):
        if isinstance(item, StrPath):
            item: Path = Path(item)
            if item in self._md_files.keys():
                return self._md_files.get(item)
            else:
                logger.error(f"Key {item} is not found")
                raise KeyError
        else:
            logger.error(f"Key {item} must be StrPath but {type(item)} received")
            raise TypeError

    get = __getitem__


md_file_dict: MdFileDict = MdFileDict()
