from pathlib import Path
from typing import Iterator

from loguru import logger

from child_dir import ChildDir
from constants import StrPath


class ChildDirDict:
    def __init__(self):
        self._child_dirs: dict[Path, ChildDir] = dict()

    def __repr__(self):
        _items: str = "\n".join([f"{k}: {v}" for k, v in self._child_dirs.items()])
        return f"{self.__class__.__name__}:\n{_items}"

    __str__ = __repr__

    def __add__(self, other):
        if isinstance(other, ChildDir):
            self._child_dirs[other.dirpath] = other
        elif isinstance(other, StrPath):
            other: Path = Path(other)
            if not other.is_dir():
                logger.error(f"Path {other} does not lead to the directory")
            else:
                _child_dir: ChildDir = ChildDir(other)
                self._child_dirs[other] = _child_dir
        else:
            logger.error(f"Added element {other} must be MdFile or StrPath but {type(other)} received")

    def __sub__(self, other):
        def _sub(path: Path):
            if path not in self._child_dirs:
                logger.error(f"File {path} is not found")
            else:
                self._child_dirs.pop(path)

        if isinstance(other, ChildDir):
            other: Path = other.dirpath
        elif isinstance(other, StrPath):
            other: Path = Path(other)
        else:
            logger.error(f"Subtracted element {other} must be MdFile or StrPath but {type(other)} received")
            return

        _sub(other)

    def __getitem__(self, item):
        if isinstance(item, StrPath):
            item: Path = Path(item)

            if item in self._child_dirs.keys():
                return self._child_dirs.get(item)
            else:
                logger.error(f"Key {item} is not found")
                raise KeyError
        else:
            logger.error(f"Key {item} must be StrPath but {type(item)} received")
            raise TypeError

    get = __getitem__

    def __iter__(self) -> Iterator[ChildDir]:
        return iter(self._child_dirs.values())


child_dir_dict: ChildDirDict = ChildDirDict()
