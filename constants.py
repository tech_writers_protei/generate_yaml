from pathlib import Path
from sys import platform
from typing import Any, Callable, Iterable, NamedTuple, TypeAlias

from loguru import logger

from errors import MissingBoolMethodError

StrPath: TypeAlias = str | Path
DictPath: TypeAlias = dict[Path, list[Path | dict[Path, list[Path, dict]]]]
DictStr: TypeAlias = dict[str, list[str | dict[str, list[str, dict]]]]
_EXTENSION: str = ".md"
_LOG_FOLDER: Path = Path(__file__).parent.joinpath("_logs")
_VERSION: str = "0.1.0"
_INDEX_FILE: str = f"_index{_EXTENSION}"
INDEX_FILE: str = f"index{_EXTENSION}"


base_path: Path = Path("/Users/user/PycharmProjects/SGW/content/common") \
    if not platform.startswith("win") else Path(r"C:\Users\tarasov-a\PycharmProjects\SGW\content\common")


def is_bool(func: Callable):
    def wrapper(*args, **kwargs):
        _self = args[0]
        if not hasattr(_self, "__bool__"):
            logger.error(f"Object {_self} does not have a bool method")
            raise MissingBoolMethodError
        elif not bool(_self):
            logger.error(f"Object {_self} is false")
            return
        else:
            return func(*args, **kwargs)
    return wrapper


def _not_none_dict(named_tuple: NamedTuple) -> dict[str, Any]:
    # noinspection PyProtectedMember
    return {
        field: getattr(named_tuple, field)
        for field in named_tuple._fields
        if getattr(named_tuple, field) is not None
    }


def _stringify_nt(named_tuple: NamedTuple):
    _dict_values: str = "\n".join(f"{k} = {v}" for k, v in _not_none_dict(named_tuple).items())
    return f"{_dict_values}"


def _reprify_nt(named_tuple: NamedTuple):
    # noinspection PyProtectedMember
    return f"<{named_tuple.__class__.__name__}({named_tuple._asdict().items()})>"


def stringify_paths(paths: Iterable[Path] = None):
    if paths is None:
        return ""
    else:
        return "\n".join(str(path) for path in paths)


def stringify_dict_items(dict_items: dict[str | Path, list[str | Path | dict]] = None):
    _: list[str] = []
    for k, values in dict_items.items():
        _str_values: str = "\n".join([f"{v}" for v in values])
        _.append(f"=====\n{k}\n=====\n{_str_values}\n")
    return "\n".join(_)


class TitleYAML(NamedTuple):
    value: str | None = None
    level: int | None = None

    def __str__(self):
        return _stringify_nt(self)

    def __repr__(self):
        return _reprify_nt(self)

    def __bool__(self):
        return any(getattr(self, field) is None for field in self._fields)

    def yamlify(self) -> dict[str, dict[str, Any]]:
        if _not_none_dict(self):
            return {"title": _not_none_dict(self)}
        else:
            return dict()


class DirYAML(NamedTuple):
    title: TitleYAML | None = None
    index: list[str] | None = None
    files: list[str] | None = None

    def __str__(self):
        return _stringify_nt(self)

    def __repr__(self):
        return _reprify_nt(self)

    def validate(self):
        if all(getattr(self, _field) is None for _field in self._fields):
            logger.error("Не найдено ни одного параметра для YAML-файла")
            # raise AllYAMLAttributesNoneError
        if (self.title.value is not None) == (self.index is not None):
            logger.error(f"{self.title.value}")
            logger.error(f"{self.index}")
            logger.error("Должен быть задан один и только один из параметров title::value и index")
            # raise NotCompatibleYAMLAttributesError

    def yamlify(self):
        _dict: dict[str, Any] = dict()
        _dict.update(self.title.yamlify())

        if self.index is not None:
            _dict["index"] = self.index
        if self.files is not None:
            _dict["files"] = self.files

        return _dict


class StorageItem(NamedTuple):
    files: list[Path] = []
    subdirs: list[Path] = []

    def __str__(self):
        return stringify_paths(self.paths)

    def __add__(self, other):
        if isinstance(other, StrPath):
            other: Path = Path(other)
            if other.is_file():
                self.files.append(other)
            elif other.is_dir():
                self.subdirs.append(other)
            else:
                logger.error(f"Invalid path {other}")
        else:
            return NotImplemented

    def __contains__(self, item):
        if isinstance(item, StrPath):
            return Path(item) in self.paths
        else:
            return False

    def add_file(self, path: Path):
        self.files.append(path)

    def add_subdir(self, path: Path):
        self.subdirs.append(path)

    @property
    def paths(self) -> list[Path]:
        return list(*self.files, *self.subdirs)

    def update_files(self, paths: Iterable[Path] = None):
        if paths is None:
            return
        self.files.clear()
        self.files.extend(paths)

    def update_subdirs(self, paths: Iterable[Path] = None):
        if paths is None:
            return
        self.subdirs.clear()
        self.subdirs.extend(paths)
