from os.path import relpath
from pathlib import Path
from typing import Iterable, Iterator

from loguru import logger

from constants import StrPath, _INDEX_FILE, is_bool
from md_file import MdFile

_TITLES: dict[str, str] = {
    "description": "Дополнительная информация",
    "oam": "Эксплуатация, администрирование и обслуживание",
    "logging": "Журналы"
}


class ChildDir:
    base_path: Path | None = None

    def __init__(
            self,
            dirpath: Path,
            weight: int = 0, *,
            files: Iterable[Path] = None,
            subdirs: Iterable[Path] = None):
        if files is None:
            files: list[Path] = []

        if subdirs is None:
            subdirs: list[Path] = []

        self._dirpath: Path = dirpath
        self._files: list[Path] = [*files]
        self._subdirs: list[Path] = [*subdirs]
        self._weight: int = weight

    def __repr__(self):
        return f"{self.__class__.__name__}: {self._dirpath}"

    __str__ = __repr__

    def __bool__(self):
        return self.__class__.base_path is not None

    def iterfiles(self) -> Iterator[Path]:
        return iter(self._files)

    def itersubdirs(self) -> Iterator[Path]:
        return iter(self._subdirs)

    @property
    def name(self):
        return self._dirpath.name

    def __int__(self):
        return self._weight

    def __add__(self, other):
        if isinstance(other, StrPath):
            other: Path = Path(other)

            if other.is_dir():
                self._subdirs.append(other)
            elif other.is_file():
                self._files.append(other)
            else:
                logger.info(f"Type is invalid")

        elif isinstance(other, MdFile):
            self._files.append(other.path)

        elif isinstance(other, self.__class__):
            self._subdirs.append(other._dirpath)

        else:
            return NotImplemented

    def _index_file(self) -> Path:
        return self._dirpath.joinpath(_INDEX_FILE)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._dirpath.parent, int(self), self.name == other._dirpath.parent, int(other), other.name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._dirpath.parent, int(self), self.name != other._dirpath.parent, int(other), other.name
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self._dirpath.parent == other._dirpath.parent:
            if int(self) == int(other):
                return self.name < other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self._dirpath.parent == other._dirpath.parent:
            if int(self) == int(other):
                return self.name > other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__) and self._dirpath.parent == other._dirpath.parent:
            if int(self) == int(other):
                return self.name <= other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__) and self._dirpath.parent == other._dirpath.parent:
            if int(self) == int(other):
                return self.name >= other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    @property
    def dirpath(self):
        return self._dirpath

    def full_files(self):
        dir_files: list[Path] = [subdir.joinpath(_INDEX_FILE) for subdir in self._subdirs]
        return [*self._files, *dir_files]

    def remove_index(self):
        try:
            self._files.remove(self._dirpath.joinpath(_INDEX_FILE))
        except ValueError:
            pass
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, {e.strerror}")
            raise

    @is_bool
    @property
    def yaml_path(self):
        return relpath(self._dirpath, self.__class__.base_path)
