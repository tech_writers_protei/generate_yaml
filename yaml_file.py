from pathlib import Path
from typing import Any, Mapping

from loguru import logger
from yaml import Loader, dump, load

from constants import StrPath
from errors import MissingRequiredYAMLKeyError


_INITIAL: dict[str, dict[str, str | int | list[str]]] = {
    "settings": {
        "title-page": "{{ProjectName}}",
        "doctype": "book",
        "toc": True,
        "figure-caption": "Рисунок",
        "chapter-signifier": False,
        "toc-title": "Содержание",
        "outlinelevels": 2,
        "title-logo-image": "images/logo.svg[top=4%,aligh=right,pdfwidth=3cm]",
        "version": "{{Version}}"
    },
    "Rights": {
        "title": {
            "value": "Юридическая информация",
            "title-files": False
        },
        "index": ["content/common_index.md"]
    }
}


class FileYAML:
    def __init__(self, path_yaml: StrPath, yaml_dict: Mapping[str, Mapping[str, str | int | list[str]]] = None):
        if yaml_dict is None:
            yaml_dict = dict()
        self._path_yaml: Path = Path(path_yaml).resolve()
        self._path_yaml.touch(exist_ok=True)
        self._yaml_dict: dict[str, dict[str, str | int | list[str]]] = yaml_dict

    def __str__(self):
        return f"{self._path_yaml}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path_yaml})>"

    def __add__(self, other):
        if isinstance(other, dict):
            self._yaml_dict.update(other)
        else:
            return NotImplemented

    __iadd__ = __add__
    __radd__ = __add__

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._yaml_dict.get(item)
        else:
            logger.error(f"Key {item} is not found")
            return NotImplemented

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._yaml_dict
        else:
            return NotImplemented

    def read(self):
        with open(self._path_yaml, "r") as f:
            loader: Loader = Loader(f)
            self._yaml_dict = load(f, loader)
        return

    def write(self, content: Mapping[str, Any] = None):
        if content is None:
            content: dict[str, Any] = self._yaml_dict

        with open(self._path_yaml, "w", encoding="cp1251") as f:
            dump(
                content,
                f,
                default_flow_style=False,
                indent=2,
                allow_unicode=True,
                sort_keys=False, explicit_start=False)
        return

    def replace(self, old_value: str, new_value: str):
        if "settings" not in self:
            logger.error("Ключ 'settings' не найден")
            raise MissingRequiredYAMLKeyError

        _value: str = "{{%s}}" % (old_value,)

        for key, value in self["settings"].items():
            if value == "{{%s}}" % (old_value,):
                self["settings"][key] = new_value
                logger.debug(f"Значение {old_value} для ключа {key} заменено на {new_value}")
